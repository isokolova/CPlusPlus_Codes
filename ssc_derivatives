#include <iostream>
#include <functional>

class Derivative // base class
{
public:
	Derivative()
		:h(1e-8) // default value
	{}

	Derivative(double h)
		:h(h)
	{
		h = 1e-8; 
	}

	~Derivative()
	{
		h = 0;
	}

	virtual double differentiate(std::function<double(double)> func, double x) = 0;

protected: // need to acess
	double h; // spacing attribute
};

class CentralDifference : public Derivative
{
public:
	using Derivative::Derivative; // inherit constructors from base class

	double differentiate(std::function<double(double)> func, double x) // SHOULD IT BE VIRTUAL??
	{
		return((func(x + h) - func(x - h)) / (2 * h)); // derivative of func at x
	}
};

class ForwardDifference : public Derivative
{
public:
	using Derivative::Derivative; // inherit the constructor from the base class
	double differentiate(std::function<double(double)> func, double x)
	{
		return ((func(x + h) - func(x)) / h); // derivative of func at x
	}
};

// define function to diffenenciate: y = x^2
// implemented as classical functinon:
const double func1(const double x) { return x*x; }
// define function to diffenenciate as lambda expression: y = x
// implemented as lambda expression:
auto func2 = [](double x) {return x; };


int main()
{
	CentralDifference CD;
	ForwardDifference FD;

	auto x = 3;

	std::cout << "Central difference scheme:" << std::endl;
	std::cout << "Derivative of y = x^2 at point x = " << x << std::endl;
	std::cout << CD.differentiate(func1, x) << std::endl;
	std::cout << "Derivative of y = x at point x = " << x << std::endl;
	std::cout << CD.differentiate(func2, x) << std::endl << std::endl;

	std::cout << "Forward difference scheme:" << std::endl;
	std::cout << "Derivative of y = x^2 at point x = " << x << std::endl;
	std::cout << FD.differentiate(func1, x) << std::endl;
	std::cout << "Derivative of y = x at point x = " << x << std::endl;
	std::cout << FD.differentiate(func2, x) << std::endl; 

	return 0;
}